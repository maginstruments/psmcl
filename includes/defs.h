#ifndef DEFS_H
#define DEFS_H

struct measurementSet{
    float Mx, My, Mz;
    unsigned int orientation;
};

struct calibrationParameters{
    float amplGainPos[2];
    float amplGainNeg[2];
    float phaseShift[2];

};

struct calibrationSampleInfo{
    int id;
    float Mx, My, Mz, volume;

};

enum measState {singleMeasurementCplt=0, fullSetComplete=1, measStopped=2, instrCalCplt=3, holderCalCplt=4, other=10};

#endif // DEFS_H
