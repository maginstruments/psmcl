#ifndef PSMCL_H
#define PSMCL_H

#include "defs.h"

// #ifdef __cplusplus
// #define EXTERNC extern "C"
// #else
// #define EXTERNC
// #endif

  


#ifdef __cplusplus
extern "C" {
#endif 
    
  

/*#ifndef BOOL 
#define BOOL unsigned int
#endif */    

    
void psm_hello();

int         psm_init(const char * portName);
void        psm_deInit();

unsigned int psm_getFirmwareVersion();
unsigned int psm_getSerialNumber();
unsigned int psm_getOperationMode();
unsigned int psm_getBatteryLevel();

void psm_newMeasurement();
void psm_doStartMeasurement(unsigned int orientationNumber);
void psm_doStopMeasurement();
void psm_getLastMeasurementData(float *Mx, float *My, float *Mz, float *orientation);
void psm_doInstrumentCalibration();
void psm_doHolderCalibration();


struct calibrationSampleInfo psm_getCalibrationSampleInfo(unsigned int sampleIndex);
void psm_setCalibrationSampleInfo(unsigned int sampleIndex, struct calibrationSampleInfo calSample);

unsigned int psm_getUsedCalSampleIndex();
void psm_setUsedCalSampleIndex(unsigned int usedCalSampleIndex);

unsigned int psm_getAutoProgression();
void psm_setAutoProgression(unsigned int newValue);

unsigned int psm_getMeasurementOrientationsIndex();
void psm_setMeasurementOrientationsIndex(unsigned int newValue);

unsigned int psm_getNrOfSpinsIndex();
void psm_setNrOfSpinsIndex(unsigned int newValue);

struct calibrationParameters psm_getCalibrationParameters();


unsigned int psm_isFullMeasurementSetCplt();
unsigned int psm_isSingleMeasurementCplt();
unsigned int psm_isInstrCalCplt();
unsigned int psm_isHolderCalCplt();
unsigned int psm_isMeasurementStopped();
unsigned int psm_didNewDataReceive();


    

#ifdef __cplusplus
}
#endif
#endif
