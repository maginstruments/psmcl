#include "psmCL.h"

#include <stdio.h>
#include <unistd.h>

int main()
{ 
    if (psm_init("/dev/ttyUSB0") != 1) {
        return -1;
    }
    
    printf("\n\n\n ************* PSM Initialized Successfully ****************** \n");
    printf(" Firmware version: %d \n", psm_getFirmwareVersion());
    printf(" Serial Number: %d \n", psm_getSerialNumber());
    printf(" Battery level: %d%% \n", psm_getBatteryLevel());
    
    printf("\n\n -------- Calibration Samples:\n");
    
    struct calibrationSampleInfo tmpInfo; 
    unsigned int i=0;
    for (i=0; i<3; i++) {
    tmpInfo = psm_getCalibrationSampleInfo(i);
    printf(" Calibration sample: %d\n", i);
    printf(" ID:%d, \t Volume [ccm]: %1f\n", tmpInfo.id, tmpInfo.volume);
    printf(" Mx: %f, \t My: %f, \t Mz: %f \n\n", tmpInfo.Mx, tmpInfo.My, tmpInfo.Mz);
    }
        
    printf(" Index of used calibration sample: %d\n", psm_getUsedCalSampleIndex());

    printf(" Automatic measurement progression is: ");
    if (psm_getAutoProgression() == 0) {
        printf("OFF\n");
    } else {
        printf("ON\n");
    }
    
    printf(" Measurement orientations: ");
    switch (psm_getMeasurementOrientationsIndex()) {
        case 0:
            printf("P1 > P2\n");
        break;
        case 1:
            printf("P1 > P2 > P5\n");
        break;
        case 2:
            printf("P1 > P2 > P3 > P4\n");
        break;
        case 3:
            printf("P1 > P2 > P3 > P4 > P5 > P6\n");
        break;
    }
    
        printf(" Number of spins: ");
    switch (psm_getNrOfSpinsIndex()) {
        case 0:
            printf("2\n");
        break;
        case 1:
            printf("5\n");
        break;
        case 2:
            printf("10\n");
        break;
    }
    
    struct calibrationParameters calParam = psm_getCalibrationParameters();
    
    printf("\n Calibration parameters:\n");
    printf(" Sensor 1: Amplitude: %1.4f, \tPhase shift: %.0f\n", (calParam.amplGainNeg[0] + calParam.amplGainPos[0]) / 2, calParam.phaseShift[0]/3.14*180.0);
    printf(" Sensor 2: Amplitude: %4.0f, \tPhase shift: %.0f\n", (calParam.amplGainNeg[1] + calParam.amplGainPos[1]) / 2, calParam.phaseShift[1]/3.14*180.0);
    
    printf("\n\n Holder calibration... "); 
    sleep(1);
    psm_doHolderCalibration();
    
    while(psm_isHolderCalCplt()==0) {   
        sleep(1);
    }
    printf("Complete!\n"); 
    
    printf("Started measurement in P1 configuration\n"); 
    psm_doStartMeasurement(1);
    while(psm_didNewDataReceive()==0) {   
        sleep(1);
    }

    float Mx, My, Mz, orientation;
    psm_getLastMeasurementData(&Mx, &My, &Mz, &orientation);
    printf("Mx \t\tMy \t\tMz \t\t Orientation\n");
    printf("%f \t%f \t%f \t %d\n", Mx, My, Mz, (int)orientation);

    printf("Rotate the sample and lower retraction holder. Measurement will start automatically.\n"); 
    while(psm_didNewDataReceive()==0) {    
        sleep(1);
    }
    psm_getLastMeasurementData(&Mx, &My, &Mz, &orientation);
    printf("%f \t%f \t%f \t %d\n", Mx, My, Mz, (int)orientation);

    psm_deInit();
    printf("Test program execution completed! \n"); 
}
      
